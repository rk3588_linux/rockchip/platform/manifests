# PX30 Linux5.10 SDK Note

---

**Versions**

[TOC]

---
## px30_linux5.10_release_v1.0.3_20221220.xml Note

```
- Fix USB PID error with usbdevice
- Fix crash when painting views without buffer with weston11
- Support bypassing Unicode when printing with busybox or coreutils
- Update to Debian11.6
- Support libcamera for rkisp
- Use clipped src/dst coordinates with vop
- Disable nak interrupt when get first isoc in token on dwc2
- Disable ss instances in park mode for usb3
```

## px30_linux5.10_release_v1.0.2_20221120.xml Note

```
- Update rk817 codec driver to solve some bugs
- Add motorcomm driver for RK631 PHY
- Upgrade weston to Version 11
- Fixes Debian11 hardware cursor issue
- reserved words for maskrom to add special parameters
- Compatible with the old loader
```

## px30_linux5.10_release_v1.0.1_20221020.xml Note

```
- Supoort Gstreamer NV12 10bit
- Update rkscripts to rework the usbdevice/resize
- Support the bootanim on bootup
- Fix weston crash when hotplugging screens
- Fix Gstreamer random hang when EOS
- Update rockchip-test tests
```

## px30_linux5.10_release_v1.0.0_20220920.xml Note

```
- The first release version
```

